#!/usr/bin/env python3
import random


def aprobar(alumnos, resultados):
    # corroborar si es mayor a 4 para aprobar
    for i in range(31):
        if alumnos[i] >= 4:
            resultados.append("APRUEBA")
        else:
            resultados.append("reprueba")


# crear matriz con notas random 4 notas por alumnos
def creacion_matriz(matriz):
    for i in range(31):
        matriz.append([])
        for j in range(4):
            numero = round(random.uniform(1, 7), 1)
            matriz[i].append(numero)


# funcion imprimir
def imprimir(matriz):
    for i in range(31):
        for j in range(4):
            print([matriz[i][j]], end='')
        print()


# funcion calculo de alumnos y asigna promedio a una lista
def promedio_alum(matriz, alumnos):
    promedio = 0
    for i in range(31):
        suma = 0
        for j in range(4):
            suma = suma + matriz[i][j]
        promedio = suma/4
        alumnos.append(promedio)
    return alumnos


# variables
matriz = []
promedio = []
alumnos = []
resultados = []
# main
if __name__ == '__main__':
    # llamar funciones
    creacion_matriz(matriz)
    imprimir(matriz)
    alumnos = promedio_alum(matriz, alumnos)
    aprobar(alumnos, resultados)
 
    # notas finales
    for i in range(31):
        print("el alumno", i, resultados[i])
        print("con nota: ", alumnos[i])

