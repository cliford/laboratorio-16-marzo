#!/usr/bin/env python3


def ordenar(largo, temporal, lista):
    # definir de inmediato la ultima posicion de la lista
    # para no tener problemas con los rangos
    temporal[0] = lista[largo - 1]
    # mover una posicion a partir del indice "1" porque el "0"
    # ya fue definido
    for i in range(0, largo-1):
        temporal[i+1] = lista[i]

# funcion que iguala la lista a la tupla(primer for)
# 2do "for" le da valores vacios a temporal para asignar
# el mismo largo


def creacion(largo, lista, tupla, temporal):
    for i in range(largo):
        lista.append(tupla[i])
    for i in range(largo):
        temporal.append(None)


# definir variables
lista = []
tupla = [True, 1, "a", False]
largo = len(tupla)
temporal = []

# llamar a las funciones

creacion(largo, lista, tupla, temporal)
ordenar(largo, temporal, lista)

# imprimir
print("Tupla: ")
print(tupla)
print("Lista: ")
print(lista)
print("Lista ordenada segun criterio:  ")
print(temporal)


