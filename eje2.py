#!/usr/bin/env python3

import random
matriz = []
numeros = []


def sum_neg(matriz):
    suma = 0
    # recorrer columnas pedidas
    # si el numero es menos a 0
    # se sumara
    for i in range(4, 8):
        for j in range(15):
            if matriz[i][j] < 0:
                suma = suma + matriz[i][j]
    return suma


def sum_colum(matriz):
    suma = 0
    # recorrer del 0 al 4
    # sumas el valor de todos los numero de las columnas
    for i in range(5):
        for j in range(15):
            suma = suma + matriz[j][i]
    return suma


def numero_menor(numeros):
    # definir variable mas grande que todos los numeros posibles
    # para evitar poblemas
    temp = 11
    # si la temporal es mayor al numero de la matriz se reemplaza
    for i in range(len(numeros)):
        if temp > numeros[i]:
            temp = numeros[i]
    return temp


# crear matriz
def creacion_matriz(matriz):
    for i in range(15):
        matriz.append([])
        for j in range(12):
            numero = random.randint(-10, 10)
            numeros.append(numero)
            matriz[i].append(numero)
    return numeros


# imprir
def imprimir(matriz):
    for i in range(15):
        for j in range(12):
            print([matriz[i][j]], end='')
        print()


if __name__ == '__main__':
    # llamar funciones
    numeros = creacion_matriz(matriz)
    imprimir(matriz)
    temp = numero_menor(numeros)
    suma_col = sum_colum(matriz)
    suma_neg = sum_neg(matriz)

    # imprimri lo pedido
    print("suma de las primeras 5 columnas: ")
    print(suma_col)
    print("numero menos: ")
    print(temp)
    print("suma de numeros negativos de la columna 5 a la 9: ")
    print(suma_neg)
